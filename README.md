# vagrant-jenkins-php
Vagrant box for running Jenkins with PHP projects according to [jenkins-php.org](http://jenkins-php.org/)

Vagrant box: https://atlas.hashicorp.com/daithi/boxes/jenkins-php

# Installation
```bash
git clone https://github.com/daithi-coombes/vagrant-jenkins-php
cd vagrant-jenkins-php
vagrant up
```
