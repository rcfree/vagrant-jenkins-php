sudo /etc/init.d/jenkins restart
sleep 10

sudo apt-get install ant -y
sudo apt-get install php5-xdebug -y
sudo apt-get install openjdk-6-jdk -y
sudo apt-get install mercurial -y

wget http://phpdox.de/releases/phpdox.phar
chmod +x phpdox.phar
sudo mv phpdox.phar /usr/local/bin/phpdox

wget https://phar.phpunit.de/phploc.phar
chmod +x phploc.phar
sudo mv phploc.phar /usr/local/bin/phploc

wget http://static.pdepend.org/php/latest/pdepend.phar
chmod +x pdepend.phar
sudo mv pdepend.phar /usr/local/bin/pdepend

wget http://static.phpmd.org/php/latest/phpmd.phar
chmod +x phpmd.phar
sudo mv phpmd.phar /usr/local/bin/phpmd

wget https://phar.phpunit.de/phpcpd.phar
chmod +x phpcpd.phar
mv phpcpd.phar /usr/local/bin/phpcpd

cd /vagrant
rm -rf jenkins-cli.jar
wget http://localhost:8080/jnlpJars/jenkins-cli.jar
cat config.xml | java -jar jenkins-cli.jar -s http://localhost:8080 create-job clinicle

java -jar jenkins-cli.jar -s http://localhost:8080 install-plugin mercurial
java -jar jenkins-cli.jar -s http://localhost:8080 restart